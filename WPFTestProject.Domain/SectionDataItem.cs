﻿namespace WPFTestProject.Domain
{
    public class SectionDataItem
    {
        public SectionDataItem(string mnemomica, string unit, string value, string description)
        {
            Mnemomica = mnemomica;
            Unit = unit;
            Value = value;
            Description = description;
        }

        public string Mnemomica { get; }
        public string Unit { get; }
        public string Value { get; }
        public string Description { get; }
    }
}
