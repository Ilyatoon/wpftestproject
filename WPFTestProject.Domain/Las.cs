﻿using WPFTestProject.Domain.Section;

namespace WPFTestProject.Domain
{
    public class Las
    {
        public Las(VersionSection version, WellSection wellInfo, DefaultSection curveInfo, DefaultSection parameterInfo, OtherSection other, ACSIISection aCSIIData)
        {
            Version = version;
            WellInfo = wellInfo;
            CurveInfo = curveInfo;
            ParameterInfo = parameterInfo;
            Other = other;
            ACSIIData = aCSIIData;
        }

        public VersionSection Version { get; }
        public WellSection WellInfo { get; }
        public DefaultSection CurveInfo { get; }
        public DefaultSection ParameterInfo { get; }
        public OtherSection Other { get; }
        public ACSIISection ACSIIData { get; }
    }
}
