﻿namespace WPFTestProject.Domain.Enum
{
    public enum SectionType
    {
        Version,
        WellInfo,
        CurveInfo,
        ParameterInfo,
        Other,
        ACSIIData,
        Unknown
    }
}
