﻿using System.Collections.Generic;
using WPFTestProject.Domain.Enum;

namespace WPFTestProject.Domain.Section
{
    public class WellSection : BaseSection
    {
        public WellSection(IList<SectionDataItem> data, double startPostion, double endPosition, double step, double nullValue, string unit) : base(data)
        {
            StartPosition = startPostion;
            EndPosition = endPosition;
            Step = step;
            NullValue = nullValue;
            Unit = unit;
        }

        public override SectionType Type => SectionType.WellInfo;

        public double StartPosition { get; }
        public double EndPosition { get; }
        public double Step { get; }
        public string Unit { get; }
        public double NullValue { get; }
    }
}
