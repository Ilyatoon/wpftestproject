﻿using System.Collections.Generic;
using WPFTestProject.Domain.Enum;

namespace WPFTestProject.Domain.Section
{
    public class VersionSection : BaseSection
    {
        public VersionSection(IList<SectionDataItem> data, string number, bool isWrap) : base(data)
        {
            Number = number;
            IsWrap = isWrap;    
        }
        public override SectionType Type => SectionType.Version;
        public string Number { get; }
        public bool IsWrap { get; }
    }
}
