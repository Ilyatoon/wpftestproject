﻿using WPFTestProject.Domain.Enum;

namespace WPFTestProject.Domain.Section
{
    public interface ISection
    {
        SectionType Type { get; }
    }
}
