﻿using System.Collections.Generic;
using WPFTestProject.Domain.Enum;

namespace WPFTestProject.Domain.Section
{
    public class ACSIISection : ISection
    {
        public ACSIISection(IList<double[]> values)
        {
            Values = values;
        }

        public IList<double[]> Values { get; }

        public SectionType Type => SectionType.ACSIIData;
    }
}
