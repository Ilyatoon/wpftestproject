﻿using System.Collections.Generic;
using WPFTestProject.Domain.Enum;

namespace WPFTestProject.Domain.Section
{
    public abstract class BaseSection : ISection
    {
        public BaseSection(IList<SectionDataItem> data) 
        { 
            Data= data;
        }

        public virtual SectionType Type { get; protected set; }
        public IList<SectionDataItem> Data { get; }
    }
}
