﻿using WPFTestProject.Domain.Enum;

namespace WPFTestProject.Domain.Section
{
    public class OtherSection : ISection
    {
        public OtherSection(string info)
        {
            Info = info;
        }

        public SectionType Type => SectionType.Other;
        public string Info { get; }
    }
}
