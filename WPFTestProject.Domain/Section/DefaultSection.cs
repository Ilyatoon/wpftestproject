﻿using System.Collections.Generic;
using WPFTestProject.Domain.Enum;

namespace WPFTestProject.Domain.Section
{
    public class DefaultSection: BaseSection
    {
        public DefaultSection(IList<SectionDataItem> data, SectionType type) : base(data)
        {
            Type = type;
        }
    }
}
