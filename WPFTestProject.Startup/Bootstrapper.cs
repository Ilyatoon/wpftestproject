﻿using Prism.Ioc;
using Prism.Unity;
using System.Windows;
using WPFTestProject.Views.Model;
using WPFTestProject.Views.Model.Abstract;
using WPFTestProject.Views.View;
using WPFTestProject.Views.View.Abstract;
using WPFTestProject.Views.ViewModel;
using WPFTestProject.Views.ViewModel.Abstract;

namespace WPFTestProject.Startup
{
    class Bootstrapper : PrismBootstrapper
    {
        public Bootstrapper()
        {
        }

        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell(DependencyObject shell)
        {
            base.InitializeShell(shell);

            Application.Current.MainWindow = (MainWindow)Shell;
            Application.Current.MainWindow.Show();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IMainWindowViewModel, MainWindowViewModel>();
            containerRegistry.Register<IMainWindowModel, MainWindowModel>();
            containerRegistry.Register<ITabControlViewModel, TabControlViewModel>();
            containerRegistry.Register<ITableControlViewModel, TableControlViewModel>();
            containerRegistry.Register<IPlotControlViewModel, PlotControlViewModel>();
        }

    }
}
