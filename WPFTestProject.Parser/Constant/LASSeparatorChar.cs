﻿namespace WPFTestProject.Parser.Constant
{
    public class LASSeparatorChar
    {
        public const char STARTSECTION = '~';
        public const char STARTCOMMENT = '#';
        public const char POINT = '.';
        public const char SPACE = ' ';
        public const char COLON = ':';
    }
}
