﻿namespace WPFTestProject.Parser.Constant
{
    public class SectionLetter
    {
        public const char VERSION = 'V';
        public const char WELLINFO = 'W';
        public const char CURVEINFO = 'C';
        public const char PARAMETERINFO = 'P';
        public const char OTHER = 'O';
        public const char ASCIIDATA = 'A';
    }
}
