﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using WPFTestProject.Parser.Builder;
using WPFTestProject.Parser.Constant;
using WPFTestProject.Domain;
using WPFTestProject.Domain.Enum;
using WinFormTestPorject.Parser.Constant;

namespace WPFTestProject.Parser
{
    public class LASParser
    {
        public LASParser() { }

        public Las Parse(string filePath)
        {
            var lasBuilder = new LasBuilder();
            string currentString;
            var sectionType = SectionType.Unknown;
            var dataArray = new List<SectionDataItem>();
            var otherInfo = new StringBuilder();

            var isParseACSII = false;

            using (var streamReader = new StreamReader(filePath))
            {

                while ((currentString = streamReader.ReadLine()) != null && !isParseACSII)
                {
                    currentString = currentString.Trim();

                    if (currentString.IndexOf(LASSeparatorChar.STARTCOMMENT) == 0)
                        continue;

                    if (currentString.IndexOf(LASSeparatorChar.STARTSECTION) == 0)
                    {
                        if(dataArray.Count > 0)
                        {
                            if (sectionType == SectionType.Version)
                                SetVersion(lasBuilder, dataArray);
                            else if (sectionType == SectionType.WellInfo)
                                SetWellInfo(lasBuilder, dataArray);                                
                            else if (sectionType == SectionType.CurveInfo)
                                lasBuilder.SetCurve(dataArray);
                            else if (sectionType == SectionType.ParameterInfo)
                                lasBuilder.SetParameter(dataArray);
                        }
                        else if(otherInfo.Length > 0)
                        {
                            lasBuilder.SetOther(otherInfo.ToString());
                        }

                        var sectionLetter = currentString[1];
                        sectionType = GetSectionType(sectionLetter);
                        dataArray = new List<SectionDataItem>();

                        if (sectionType == SectionType.ACSIIData)
                            isParseACSII = true;
                    }
                    else
                    {
                        if(sectionType != SectionType.Other)
                            dataArray.Add(ParseSectionDataItem(currentString));
                        else
                            otherInfo.Append($"{currentString}\n");
                    }                  
                }

                var aCSIIValuesList = new List<double[]>();
                while ((currentString = streamReader.ReadLine()) != null)
                {
                    currentString = currentString.Trim();
                    var array = currentString.Split(new char[]{LASSeparatorChar.SPACE}, StringSplitOptions.RemoveEmptyEntries);

                    var values = Array.ConvertAll(array, new Converter<string, double>(s => double.Parse(s, CultureInfo.InvariantCulture)));
                    aCSIIValuesList.Add(values);
                }
                lasBuilder.SetACSII(aCSIIValuesList);

                return lasBuilder.GetLas();
            }
        }

        private SectionDataItem ParseSectionDataItem(string currentString)
        {
            var pointIndex = currentString.IndexOf(LASSeparatorChar.POINT);
            var mnemomica = pointIndex != -1 ?  currentString.Substring(0, pointIndex).Trim() : string.Empty;

            var remainingString = currentString.Substring(pointIndex+1);

            var spaceIndex = remainingString.IndexOf(LASSeparatorChar.SPACE);
            var unit = spaceIndex != -1 ? remainingString.Substring(0, spaceIndex).Trim() : string.Empty;

            remainingString = remainingString.Substring(spaceIndex + 1);

            var colonIndex = remainingString.IndexOf(LASSeparatorChar.COLON);
            var _value = colonIndex != -1 ? remainingString.Substring(0, colonIndex).Trim() : string.Empty;
            var description = colonIndex != remainingString.Length -1 ? remainingString.Substring(colonIndex+1) : string.Empty;

            return new SectionDataItem(mnemomica, unit, _value, description);
        }

        private void SetVersion(ILasBuilder lasBuilder, IList<SectionDataItem> list)
        {
            var number = GetAndRemoveRequiredField(list, VersionRequiredField.VERS).Value;
            var isWrap = GetAndRemoveRequiredField(list, VersionRequiredField.WRAP).Value == "YES";

            lasBuilder.SetVersion(list, number, isWrap);
        }

        private void SetWellInfo(ILasBuilder lasBuilder, IList<SectionDataItem> list)
        {
            var startPostion = Convert.ToDouble(GetAndRemoveRequiredField(list, WellRequiredField.STRT).Value, CultureInfo.InvariantCulture);
            var unit = list.FirstOrDefault(x => x.Mnemomica == WellRequiredField.STOP).Unit;
            var endPostion = Convert.ToDouble(GetAndRemoveRequiredField(list, WellRequiredField.STOP).Value, CultureInfo.InvariantCulture);
            var step = Convert.ToDouble(GetAndRemoveRequiredField(list, WellRequiredField.STEP).Value, CultureInfo.InvariantCulture);
            var nullValue = Convert.ToDouble(GetAndRemoveRequiredField(list, WellRequiredField.NULL).Value, CultureInfo.InvariantCulture);

            lasBuilder.SetWell(list, startPostion, endPostion, step, nullValue, unit);
        }

        private SectionType GetSectionType(char letter)
        {
            switch (letter)
            {
                case SectionLetter.VERSION:
                    return SectionType.Version;
                case SectionLetter.WELLINFO:
                    return SectionType.WellInfo;
                case SectionLetter.CURVEINFO:
                    return SectionType.CurveInfo;
                case SectionLetter.PARAMETERINFO:
                    return SectionType.ParameterInfo;
                case SectionLetter.OTHER:
                    return SectionType.Other;
                case SectionLetter.ASCIIDATA:
                    return SectionType.ACSIIData;
            }

            return SectionType.Unknown;
        }

        private SectionDataItem GetAndRemoveRequiredField(IList<SectionDataItem> list, string requiredField)
        {
            var field = list.FirstOrDefault(x => x.Mnemomica == requiredField);
            list.Remove(field); 
            return field;
        }
    }
}
