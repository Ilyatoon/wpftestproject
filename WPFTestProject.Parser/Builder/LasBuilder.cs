﻿using System.Collections.Generic;
using WPFTestProject.Domain;
using WPFTestProject.Domain.Enum;
using WPFTestProject.Domain.Section;

namespace WPFTestProject.Parser.Builder
{
    public class LasBuilder : ILasBuilder
    {
        private VersionSection _version;
        private WellSection _wellInfo;
        private DefaultSection _curveInfo;
        private DefaultSection _parameterInfo;
        private OtherSection _other;
        private ACSIISection _aCSIIData;

        public void SetVersion(IList<SectionDataItem> data, string number, bool isWrap)
        {
            _version = new VersionSection(data, number, isWrap);
        }

        public void SetWell(IList<SectionDataItem> data, double startPosition, double endPosition, double step, double nullValue, string unit)
        {
            _wellInfo = new WellSection(data, startPosition, endPosition, step, nullValue, unit);
        }
        public void SetCurve(IList<SectionDataItem> data)
        {
            _curveInfo = new DefaultSection(data, SectionType.CurveInfo);
        }

        public void SetParameter(IList<SectionDataItem> data)
        {
            _parameterInfo = new DefaultSection(data, SectionType.ParameterInfo);
        }

        public void SetOther(string info)
        {
            _other = new OtherSection(info);
        }

        public void SetACSII(IList<double[]> values)
        {
            _aCSIIData = new ACSIISection(values);
        }

        public Las GetLas()
        {
            return new Las(_version, _wellInfo, _curveInfo, _parameterInfo, _other, _aCSIIData);
        }
    }
}
