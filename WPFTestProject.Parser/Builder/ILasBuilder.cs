﻿using System.Collections.Generic;
using WPFTestProject.Domain;

namespace WPFTestProject.Parser.Builder
{
    public interface ILasBuilder
    {
        Las GetLas();
        void SetACSII(IList<double[]> values);
        void SetCurve(IList<SectionDataItem> data);
        void SetOther(string info);
        void SetParameter(IList<SectionDataItem> data);
        void SetVersion(IList<SectionDataItem> data, string number, bool isWrap);
        void SetWell(IList<SectionDataItem> data, double startPosition, double endPosition, double step, double nullValue, string unit);
    }
}