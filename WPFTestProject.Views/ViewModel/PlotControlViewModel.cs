﻿using OxyPlot;
using WPFTestProject.Domain;
using WPFTestProject.Views.ViewModel.Abstract;

namespace WPFTestProject.Views.ViewModel
{
    public class PlotControlViewModel : NotificationObject, IPlotControlViewModel
    {
        public PlotControlViewModel(PlotModel plotModel)
        {
            PlotModel = plotModel;
            PlotModel.PlotAreaBorderThickness = new OxyThickness(1, 0, 1, 1);
        }

        public PlotModel PlotModel { get; private set; }

        public void SetPlot(Las las)
        {
            ResetPlotModel();
            var plotShaper = new PlotShaper(las.CurveInfo.Data);

            var axisY = plotShaper.GetYAxis(las.WellInfo);
            PlotModel.Axes.Add(axisY);

            var nullValue = las.WellInfo.NullValue;

            var firstAxisX = plotShaper.GetXAxis(0);
            var secondAxisX = plotShaper.GetXAxis(1);

            var firstLineSeries = plotShaper.GetLineSeries(0);
            var secondLineSeries = plotShaper.GetLineSeries(1);

            PlotModel.Axes.Add(firstAxisX);
            PlotModel.Axes.Add(secondAxisX);

            foreach (var item in las.ACSIIData.Values)
            {
                if (item[1] != nullValue)
                    firstLineSeries.Points.Add(new DataPoint(item[1], item[0]));

                if (item[2] != nullValue)
                    secondLineSeries.Points.Add(new DataPoint(item[2], item[0]));
            }
            PlotModel.Series.Add(secondLineSeries);
            PlotModel.Series.Add(firstLineSeries);
            PlotModel.InvalidatePlot(true);
        }

        private void ResetPlotModel()
        {
            PlotModel.Series.Clear();
            PlotModel.Axes.Clear();
            PlotModel.ResetAllAxes();
        }
    }
}
