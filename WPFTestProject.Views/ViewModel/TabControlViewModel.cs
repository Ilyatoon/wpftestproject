﻿using System.Collections.ObjectModel;
using WPFTestProject.Domain.Section;
using WPFTestProject.Domain;
using WPFTestProject.Views.ViewModel.Abstract;

namespace WPFTestProject.Views.ViewModel
{
    public class TabControlViewModel : NotificationObject, ITabControlViewModel
    {
        public TabControlViewModel(ITableControlViewModel wellInfoVieWModel, ITableControlViewModel versionVieWModel, ITableControlViewModel parameterViewModel,
            ITableControlViewModel curveViewModel, IPlotControlViewModel plotControlViewModel)
        {
            VersionVieWModel = versionVieWModel;
            WellInfoVieWModel = wellInfoVieWModel;
            ParameterInfoVieWModel = parameterViewModel;
            CurveInfoVieWModel = curveViewModel;
            PlotControlViewModel = plotControlViewModel;
        }

        public ITableControlViewModel VersionVieWModel { get; private set; }
        public ITableControlViewModel WellInfoVieWModel { get; private set; }
        public ITableControlViewModel ParameterInfoVieWModel { get; private set; }
        public ITableControlViewModel CurveInfoVieWModel { get; private set; }
        public IPlotControlViewModel PlotControlViewModel { get; set; }

        private bool _tabVisible;
        public bool TabVisible
        {
            get { return _tabVisible; }
            set
            {
                _tabVisible = value;
                RaisePropertyChanged(nameof(TabVisible));
            }
        }

        private string _otherInfo;
        public string OtherInfo
        {
            get { return _otherInfo; }
            set
            {
                _otherInfo = value;
                RaisePropertyChanged(nameof(OtherInfo));
            }
        }

        public void SetWellInfo(WellSection wellInfo)
        {
            var well = new ObservableCollection<SectionDataItem>(wellInfo.Data);
            well.Add(new SectionDataItem("STRT", wellInfo.Unit, wellInfo.StartPosition.ToString(), "Стартовая позиция"));
            well.Add(new SectionDataItem("STOP", wellInfo.Unit, wellInfo.EndPosition.ToString(), "Конечная позиция"));
            well.Add(new SectionDataItem("STEP", wellInfo.Unit, wellInfo.Step.ToString(), "Шаг"));
            well.Add(new SectionDataItem("NULL", wellInfo.Unit, wellInfo.NullValue.ToString(), "Нулевое значение"));

            WellInfoVieWModel.Data = well;
        }

        public void SetVersion(VersionSection versionInfo)
        {
            var version = new ObservableCollection<SectionDataItem>()
            {
                new SectionDataItem("VERS", string.Empty, versionInfo.Number, "Номер версии"),
                new SectionDataItem("WRAP",  string.Empty,versionInfo.IsWrap.ToString(), "Формат данных в секции")
            };

            VersionVieWModel.Data = version;
        }

        public void SetParameterInfo(DefaultSection parameterInfo)
        {
            ParameterInfoVieWModel.Data = new ObservableCollection<SectionDataItem>(parameterInfo.Data);
        }
        public void SetCurveInfo(DefaultSection curveInfo)
        {
            CurveInfoVieWModel.Data = new ObservableCollection<SectionDataItem>(curveInfo.Data);
        }

        public void SetOtherInfo(OtherSection other)
        {
            if (other != null)
                OtherInfo = other.Info;
        }

        public void SetPlot(Las las)
        {
            PlotControlViewModel.SetPlot(las);
        }
    }
}
