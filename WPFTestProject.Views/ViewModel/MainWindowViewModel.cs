﻿using WPFTestProject.Views.Model.Abstract;
using WPFTestProject.Views.ViewModel.Abstract;

namespace WPFTestProject.Views.ViewModel
{
    public class MainWindowViewModel : NotificationObject, IMainWindowViewModel
    {
        private readonly IMainWindowModel _model;
      
        public MainWindowViewModel(IMainWindowModel model, ITabControlViewModel tabControlViewModel)
        {
            _model = model;
            TabControlViewModel = tabControlViewModel;
        }
        public ITabControlViewModel TabControlViewModel { get; private set; }

        
        public void OpenFile(string filePath)
        {
            var las = _model.ParseLASFile(filePath);
            TabControlViewModel.SetWellInfo(las.WellInfo);
            TabControlViewModel.SetVersion(las.Version);
            TabControlViewModel.SetParameterInfo(las.ParameterInfo);
            TabControlViewModel.SetCurveInfo(las.CurveInfo);
            TabControlViewModel.SetOtherInfo(las.Other);
            TabControlViewModel.SetPlot(las);

            TabControlViewModel.TabVisible = true;
        }
    }
}
