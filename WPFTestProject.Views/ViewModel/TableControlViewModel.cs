﻿using System.Collections.ObjectModel;
using WPFTestProject.Domain;
using WPFTestProject.Views.ViewModel.Abstract;

namespace WPFTestProject.Views.ViewModel
{   
    public class TableControlViewModel : NotificationObject, ITableControlViewModel
    {
        public TableControlViewModel()
        {
            Data = new ObservableCollection<SectionDataItem>();
        }

        private ObservableCollection<SectionDataItem> _data;
        public ObservableCollection<SectionDataItem> Data
        {
            get { return _data; }
            set
            {
                _data = value;
                RaisePropertyChanged(nameof(Data));
            }
        }
    }
}
