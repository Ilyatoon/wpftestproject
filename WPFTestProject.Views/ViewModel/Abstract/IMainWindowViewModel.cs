﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFTestProject.Views.ViewModel.Abstract
{
    public interface IMainWindowViewModel : IViewModel
    {
        void OpenFile(string filePath);
    }
}
