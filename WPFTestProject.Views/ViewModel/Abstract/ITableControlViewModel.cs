﻿using System.Collections.ObjectModel;
using WPFTestProject.Domain;

namespace WPFTestProject.Views.ViewModel.Abstract
{
    public interface ITableControlViewModel : IViewModel
    {
        ObservableCollection<SectionDataItem> Data { get; set; }
    }
}
