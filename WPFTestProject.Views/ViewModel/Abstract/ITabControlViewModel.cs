﻿using WPFTestProject.Domain.Section;
using WPFTestProject.Domain;

namespace WPFTestProject.Views.ViewModel.Abstract
{
    public interface ITabControlViewModel : IViewModel
    {
        ITableControlViewModel CurveInfoVieWModel { get; }
        string OtherInfo { get; set; }
        ITableControlViewModel ParameterInfoVieWModel { get; }
        IPlotControlViewModel PlotControlViewModel { get; set; }
        bool TabVisible { get; set; }
        ITableControlViewModel VersionVieWModel { get; }
        ITableControlViewModel WellInfoVieWModel { get; }

        void SetCurveInfo(DefaultSection curveInfo);
        void SetOtherInfo(OtherSection other);
        void SetParameterInfo(DefaultSection parameterInfo);
        void SetPlot(Las las);
        void SetVersion(VersionSection versionInfo);
        void SetWellInfo(WellSection wellInfo);
    }
}
