﻿using OxyPlot;
using WPFTestProject.Domain;

namespace WPFTestProject.Views.ViewModel.Abstract
{
    public interface IPlotControlViewModel : IViewModel
    {
        PlotModel PlotModel { get; }
        void SetPlot(Las las);
    }
}
