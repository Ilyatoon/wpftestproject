﻿using Microsoft.Win32;
using System.Windows;
using WPFTestProject.Views.View.Abstract;
using WPFTestProject.Views.ViewModel.Abstract;

namespace WPFTestProject.Views.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IMainWindow
    {
        private readonly IMainWindowViewModel _viewModel;
        public MainWindow(IMainWindowViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "LAS files(*.las)|*.las|All files(*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
                _viewModel.OpenFile(openFileDialog.FileName);
        }
    }
}
