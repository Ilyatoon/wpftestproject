﻿using OxyPlot;
using System.Windows.Controls;
using System.Windows.Input;
using WPFTestProject.Views.View.Abstract;

namespace WPFTestProject.Views.View
{
    /// <summary>
    /// Логика взаимодействия для PlotControl.xaml
    /// </summary>
    public partial class PlotControl : UserControl, IPlotControl
    {
        public PlotControl()
        {
            InitializeComponent();
            PlotView.Controller = customController;
        }

        public PlotController customController;

        private void PlotView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var key = (e.Delta < 0) ? OxyKey.Left : OxyKey.Right;

            var oxyEventArgs = new OxyKeyEventArgs
            {
                Key = key
            };

            switch (key)
            {
                case OxyKey.Left:
                    PlotCommands.PanUp.Execute(PlotView, customController, oxyEventArgs);
                    break;

                case OxyKey.Right:
                    PlotCommands.PanDown.Execute(PlotView, customController, oxyEventArgs);
                    break;
            }

            e.Handled = true;
        }
    }
}
