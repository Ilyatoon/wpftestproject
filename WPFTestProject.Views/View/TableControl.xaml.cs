﻿using System.Windows.Controls;
using WPFTestProject.Views.View.Abstract;

namespace WPFTestProject.Views.View
{
    /// <summary>
    /// Логика взаимодействия для TableControl.xaml
    /// </summary>
    public partial class TableControl : UserControl, ITabControl
    {
        public TableControl()
        {
            InitializeComponent();
        }
    }
}
