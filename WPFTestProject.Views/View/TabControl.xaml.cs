﻿using System.Windows.Controls;
using WPFTestProject.Views.View.Abstract;

namespace WPFTestProject.Views.View
{
    /// <summary>
    /// Логика взаимодействия для TabControl.xaml
    /// </summary>
    public partial class TabControl : UserControl, ITabControl
    {
        public TabControl()
        {
            InitializeComponent();
        }
    }
}
