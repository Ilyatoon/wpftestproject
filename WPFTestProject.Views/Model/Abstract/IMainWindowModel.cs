﻿using WPFTestProject.Domain;

namespace WPFTestProject.Views.Model.Abstract
{
    public interface IMainWindowModel : IModel
    {
        Las ParseLASFile(string filePath);
    }
}
