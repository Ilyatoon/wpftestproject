﻿using WPFTestProject.Domain;
using WPFTestProject.Parser;
using WPFTestProject.Views.Model.Abstract;

namespace WPFTestProject.Views.Model
{
    public class MainWindowModel : IMainWindowModel
    {
        private readonly LASParser _lASParser;
        public MainWindowModel()
        {
            _lASParser = new LASParser();
        }

        public Las ParseLASFile(string filePath)
        {
            return _lASParser.Parse(filePath);
        }
    }
}
