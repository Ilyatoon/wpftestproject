﻿using OxyPlot;
namespace WPFTestProject.Views.ViewData
{
    public class PlotAxisTemplate
    {
        public PlotAxisTemplate(string key, string header, OxyColor color, LineStyle majorLineStyle, LineStyle minorLineStyle)
        {
            Key = key;
            Header = header;
            Color = color;
            MajorLineStyle = majorLineStyle;
            MinorLineStyle = minorLineStyle;
        }

        public string Key { get; }
        public string Header { get; }
        public OxyColor Color { get; }
        public LineStyle MajorLineStyle { get; }
        public LineStyle MinorLineStyle { get; }
    }
}
