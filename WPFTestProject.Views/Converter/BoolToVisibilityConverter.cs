﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace WPFTestProject.Views.Converter
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public BoolToVisibilityConverter(){ }

        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            bool bValue = false;
            if (value is bool)
            {
                bValue = (bool)value;
            }
            else if (value is Nullable<bool>)
            {
                Nullable<bool> tmp = (Nullable<bool>)value;
                bValue = tmp.HasValue ? tmp.Value : false;
            }
            return (bValue) ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (value is Visibility)
            {
                return (Visibility)value == Visibility.Visible;
            }
            else
            {
                return false;
            }
        }
    }
}
