﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System.Collections.Generic;
using WPFTestProject.Domain;
using WPFTestProject.Domain.Section;
using WPFTestProject.Views.ViewData;

namespace WPFTestProject.Views
{
    public class PlotShaper
    {
        private readonly PlotAxisTemplate[] _plotAxisXTemplates;
        private const string keyAxisY = "axisY";
        public PlotShaper(IList<SectionDataItem> curveData) 
        {
            _plotAxisXTemplates= GetAxisTemplates(curveData);
        }
        public LinearAxis GetYAxis(WellSection wellInfo)
        {
            var startValue = wellInfo.StartPosition;
            var endValue = wellInfo.EndPosition;

            return new LinearAxis()
            {
                Title = "Глубина",
                AxisTitleDistance = 15,
                AbsoluteMinimum = startValue,
                AbsoluteMaximum = endValue + 5,
                Minimum = startValue,
                Maximum = startValue + 20,
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.DashDotDot,
                EndPosition = 0,
                StartPosition = 1,
                MinorTickSize = 0,
                MajorTickSize = 25,
                TickStyle = TickStyle.Outside,
                IntervalLength = 2,
                MajorStep = 2,
                Key = keyAxisY,
                IsAxisVisible = true,
                IsPanEnabled = true,
                IsZoomEnabled = false
            };
        }

        public LinearAxis GetXAxis(int index)
        {
            return new LinearAxis()
            {
                Title = _plotAxisXTemplates[index].Header,
                Position = AxisPosition.Top,
                MinorTickSize = 0,
                IsPanEnabled = false,
                IsAxisVisible = true,
                IsZoomEnabled = false,
                AxislineStyle = LineStyle.Solid,
                Key = _plotAxisXTemplates[index].Key,
                PositionTier = index,
                FontSize = 13,
                AxislineThickness = 2,
                MajorTickSize =5,
                AxislineColor = _plotAxisXTemplates[index].Color,
                TextColor = _plotAxisXTemplates[index].Color,
                TicklineColor = _plotAxisXTemplates[index].Color,
                MajorGridlineStyle = _plotAxisXTemplates[index].MajorLineStyle,
                MinorGridlineStyle = _plotAxisXTemplates[index].MinorLineStyle,
            };
        }

        public LineSeries GetLineSeries(int index)
        {
            return new LineSeries()
            {
                Color = _plotAxisXTemplates[index].Color,
                LineStyle = LineStyle.Solid,
                MarkerSize = 3,
                MarkerFill = _plotAxisXTemplates[index].Color,
                YAxisKey = keyAxisY,
                XAxisKey = _plotAxisXTemplates[index].Key,
                IsVisible = true,
                Decimator = Decimator.Decimate,
                MarkerType = MarkerType.None,
                EdgeRenderingMode = EdgeRenderingMode.PreferSpeed
            };
        }

        private PlotAxisTemplate[] GetAxisTemplates(IList<SectionDataItem> curveData)
        {
            return new PlotAxisTemplate[]
            {
                new PlotAxisTemplate("firstAxisX", GetAxisHeader(0, curveData), OxyColors.Tomato, LineStyle.Solid, LineStyle.DashDotDot),
                new PlotAxisTemplate("secondAxisX", GetAxisHeader(1, curveData), OxyColors.Blue, LineStyle.None, LineStyle.None)
            };
        }

        private string GetAxisHeader(int index, IList<SectionDataItem> curveData)
        {
            var title = string.Empty;

            if (curveData.Count > index)
                title = $"{curveData[index + 1].Mnemomica}, {curveData[index + 1].Unit}";

            return title;
        }
    }
}
